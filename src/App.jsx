import "./App.css";
import { useState, useCallback } from "react";
import { useMovies } from "./hooks/useMovies";
import { Movies } from "./components/movies";
import { useSearch } from "./hooks/useSearch";
import debounce from "just-debounce-it";

function App() {
  const [sort, setSort] = useState(false);
  const { search, updateSearch, error, isFirstInput } = useSearch();
  const { movies, getMovies, loading } = useMovies({ search, sort });

  const debouncedGetMovies = useCallback(
    debounce((search) => {
      console.log("search", search);
      getMovies({ search });
    }, 350),
    [getMovies]
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    getMovies({ search });
  };

  const handleSort = () => {
    setSort(!sort);
  };

  const handleChange = (event) => {
    const newSearch = event.target.value;
    if (search.startsWith(" ")) return;
    updateSearch(newSearch);
    debouncedGetMovies(newSearch);
  };

  return (
    <div className="page">
      <header>
        <h1>Movies Browser</h1>
        <form action="" className="form" onSubmit={handleSubmit}>
            {" "}
            <input
              onChange={handleChange}
              name="query"
              type="text"
              value={search}
              placeholder="Cars, Spiderman, Madagascar..."
            />

          <button type="submit">Search</button>
  
          <input
            type="checkbox"
            onChange={handleSort}
            checked={sort}
            data-hover="Alphabetical filter"
          />
                  <label>Order by year</label>
        </form>
        {error && <p style={{ color: "red", zIndex:"2" }}>{error}</p>}
      </header>
      <main>{loading ? <p>Loading...</p> : <Movies movies={movies} />}</main>
    </div>
  );
}

export default App;
