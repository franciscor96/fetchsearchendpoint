import { useState, useCallback, useMemo } from "react";
import { ColorExtractor } from "react-color-extractor";




function ListOfMovies({ movies }) {

  const [elementColors, setElementColors] = useState({});

  const handleMouseEnter = (elementId) => {
    setHover(elementId);
  };
  
  const handleMouseLeave = () => {
    setHover(null);
  };
  
  
  const handleColorsExtraction = useMemo(() => (colors, elementId) => {
    setElementColors((prevColors) => ({
      ...prevColors,
      [elementId]: colors.slice(0, 5),
    }));
  }, [movies]);
  
  

  return (
    <ul className="movies">
      {movies.map((movie) => (
        <li className="movie" key={movie.id + "MovieKey"}
        style={{
          border: `2px solid rgba(${elementColors[movie.id]?.[1]}, 0.7)`,
           background: `radial-gradient(circle, rgba(${elementColors[movie.id]?.[3]}, 0.5) 20%, rgba(${elementColors[movie.id]?.[3]}, 0.5) 50%, rgba(${elementColors[movie.id]?.[1]}, 0.5) 105%)`,
           filter: `drop-shadow(6px 4px 5px rgba(${elementColors[movie.id]?.[1]}, 0.3))`,
           transition: "0.3s all ease"
        }}
        >
          <h3 style={{color: `rgba(${elementColors[movie.id]?.[1]}, 1)`}}>{movie.title}</h3>
          <p>{movie.year}</p>
          <ColorExtractor rgb  getColors={(colors) => handleColorsExtraction(colors, movie.id)}>
          <img src={movie.poster} alt={movie.Title + "image"} />
          </ColorExtractor>
        </li>
      ))}
    </ul>
  );
}

function NoMoviesResults() {
  return <p>No Movies Found</p>;
}

export function Movies({ movies }) {

  const hasMovies = movies?.length > 0;

  return (
    hasMovies 
    ? <ListOfMovies movies={movies}/> 
    : <NoMoviesResults/>
  )
}
