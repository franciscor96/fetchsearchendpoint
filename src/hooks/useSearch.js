import { useState, useEffect, useRef } from "react";
export function useSearch () {
    const [search, updateSearch] = useState('')
    const [error, setError] = useState(null);
    const isFirstInput = useRef(true)
  
    useEffect(() => {
     
    if (isFirstInput.current) {
        isFirstInput.current = search === ""
        return
    }

      if (search === "") {
        setError("Cannot search empty field");
        return;
      }
  
      if (search.match(/^\d+$/)) {
        setError("Cannot contain numbers");
        return;
      }
  
      if (search.length < 3) {
        setError("Should contain unleass 3 characters");
        return;
      }
  
      setError(null);
    }, [search]);
  
    return {search, updateSearch, error}
  
  }